Number.prototype.even = function () {
  return this.valueOf() % 2 === 0;
};

String.prototype.toCapitalize = function (str) {
  let arr = str.split(" ");

  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].substring(1);
  }

  return arr.join(" ");
}

Date.prototype.dateEnFrancais = function (date) {
  const months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"];
  const days = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]
  console.log(date.getFullYear());
  console.log(date.getMonth());
  console.log(date.getDay());

  return days[date.getDay()] + " " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();

}
